# The universal unipycation bootstrap and shared uni.py.

This code accompanies the paper:

Approaches to interpreter composition, by Edd Barrett, Carl Friedrich Bolz,
and Laurence Tratt.

# Contents

This repo contains:

 * A bootstrapper for the unipycation family of VMs:
   * Unipycation.
   * Jythog (aka, Jython-tuProlog).
   * Swithon (aka, CPython-SWI) and SwiPy (aka, PyPy-SWI).
 * VM independent implementation of uni.py
 * Tests for the above.
 * VM independent tests for CoreEngine and Objects.

## Bootstrapping

The bootstrapper here will prepare several VMs. If you just want to
use unipycation, then use the standalone bootstrapper found here:
https://bitbucket.org/softdevteam/unipycation

If you want to run the benchmarks from the paper, then you also want to look
at the unipycation-benchmarks repo instead of this repo:
https://bitbucket.org/softdevteam/unipycation-benchmarks

So assuming this is the repo you want, you will need the `sh` and
`vcstools` Python modules installed.

Run:

```
$ make
```

This will fetch the required sources and build them. Needless to say, your
system will need to be on the internet.

Note that fetching the unipycation sources takes a while.

If you have PyPy installed, you can bootstrap unipycation faster by setting
the environment variable `TRANSLATE_WITH` to your PyPy binary.

To choose which VMs to bootstrap, pass the VMS variable to make, e.g.:

```
$ make VMS="unipycation jythog"
```

Available VMs are 'unipycation', "jythog" and "swithon".

## Running

The bootstrapper emits programs in the following locations:

 * unipycation/pypy/goal/unipycation
 * swithon/bin/swithon
 * jythog/bin/jythog
