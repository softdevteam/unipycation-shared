#!/usr/bin/env python2.7
import os, os.path, sys, importlib, sh

SCRIPT_PATH = os.path.abspath(__file__)
SCRIPT_DIR = os.path.dirname(SCRIPT_PATH)

# Can override versions via environment

SWITHON_VCS = "git"
SWITHON_REPO = "https://bitbucket.org/softdevteam/swithon.git"
SWITHON_DIR = os.path.join(SCRIPT_DIR, "swithon")
SWITHON_BOOTSTRAP_PY = os.path.join(SWITHON_DIR, "bootstrap.py")
SWITHON_VERSION = os.environ.get("SWITHON_VERSION", "master")

JYTHOG_VCS = "git"
JYTHOG_REPO = "https://bitbucket.org/softdevteam/jythog.git"
JYTHOG_DIR = os.path.join(SCRIPT_DIR, "jythog")
JYTHOG_BOOTSTRAP_PY = os.path.join(JYTHOG_DIR, "bootstrap.py")
JYTHOG_VERSION = os.environ.get("JYTHOG_VERSION", "master")

UNIPY_VCS = "hg"
UNIPY_REPO = "https://bitbucket.org/softdevteam/unipycation"
UNIPY_DIR = os.path.join(SCRIPT_DIR, "unipycation")
UNIPY_BOOTSTRAP_PY = os.path.join(UNIPY_DIR, "bootstrap.py")
UNIPY_VERSION = os.environ.get("UNIPYCATION_VERSION", "unipycation")

#
# FETCH
#

def fetch_swithon():
    import vcstools
    vcs = vcstools.get_vcs_client(SWITHON_VCS, SWITHON_DIR)
    if not os.path.exists(SWITHON_DIR):
        print("Cloning Swithon: version=%s" % SWITHON_VERSION)
        vcs.checkout(SWITHON_REPO, version=SWITHON_VERSION)
    else:
        print("Updating existing Swithon to version: %s" % SWITHON_VERSION)
        vcs.update(version=SWITHON_VERSION, force_fetch=True)

def fetch_jythog():
    import vcstools
    vcs = vcstools.get_vcs_client(JYTHOG_VCS, JYTHOG_DIR)
    if not os.path.exists(JYTHOG_DIR):
        print("Cloning Jythog: version=%s" % JYTHOG_VERSION)
        vcs.checkout(JYTHOG_REPO, version=JYTHOG_VERSION)
    else:
        print("Updating existing Jythog to version: %s" % JYTHOG_VERSION)
        vcs.update(version=JYTHOG_VERSION, force_fetch=True)

def fetch_unipy():
    import vcstools
    vcs = vcstools.get_vcs_client(UNIPY_VCS, UNIPY_DIR)
    if not os.path.exists(UNIPY_DIR):
        print("Cloning unipycation: version=%s" % UNIPY_VERSION)
        vcs.checkout(UNIPY_REPO, version=UNIPY_VERSION)
    else:
        print("Updating existing unipycation to version: %s" % UNIPY_VERSION)
        vcs.update(version=UNIPY_VERSION)
#
# BOOTSTRAP
#

def bootstrap_swithon(target):
    if target in ["fetch", "all"]:
        fetch_swithon()

    sh.Command(sys.executable)(
            SWITHON_BOOTSTRAP_PY, target, SCRIPT_DIR,
            _out=sys.stdout, _err=sys.stderr)

def bootstrap_jythog(target):
    if target in ["fetch", "all"]:
        fetch_jythog()

    sh.Command(sys.executable)(
            JYTHOG_BOOTSTRAP_PY, target, SCRIPT_DIR,
            _out=sys.stdout, _err=sys.stderr)

def bootstrap_unipy(target):
    if target in ["fetch", "all"]:
        fetch_unipy()

    sh.Command(sys.executable)(
            UNIPY_BOOTSTRAP_PY, target, SCRIPT_DIR,
            _out=sys.stdout, _err=sys.stderr)

#
# MAIN
#

def usage():
    print("\nUsage:")
    print("  bootstrap.py target vm_1 ... vm_n")
    print("\nWhere possible vms are: 'unipycation', 'jythog', 'swithon'")
    print("And possible targets are: fetch, build, configure, all.")
    sys.exit(1)

# vm_name -> (bootstrap_func, dir)
VMTAB = {
    "swithon" : (bootstrap_swithon, SWITHON_DIR),
    "unipycation" : (bootstrap_unipy, UNIPY_DIR),
    "jythog" : (bootstrap_jythog, JYTHOG_DIR),
}

if __name__ == "__main__":

    try:
        target = sys.argv[1]
    except IndexError:
        usage()

    if target not in ["fetch", "build", "configure", "all"]:
        print("Bad target")
        usage()

    vms = sys.argv[2:]
    if not vms:
        # bootstrap all VMs
        vms = VMTAB.keys()

    # Check VMs look sane up front
    for vm in vms:
        try:
            assert vm in VMTAB.keys()
        except AssertionError:
            print("Unknown VM: %s" % vm)
            sys.exit(666)

    for vm in vms:
        print(72 * "-")
        print("Bootstrapping %s (target=%s)" % (vm, target))
        print(72 * "-")
        VMTAB[vm][0](target)
