#!/usr/bin/env python2.7

import os, os.path, sys, sh

def usage():
    print("translate_unipycation.py <unipycation-dir> <pyrolog_dir> <output-binary-name> [extra-target-rpython-args]")
    sys.exit(1)

try:
    BOOTSTRAP_PY = os.environ["TRANSLATE_WITH"]
except KeyError:
    BOOTSTRAP_PY = sys.executable

try:
    UNI_DIR = sys.argv[1]
    PYRO_DIR = sys.argv[2]
    OUTNAME = sys.argv[3]
except IndexError:
    usage()

# anything left on the command line
extra_rpython_args = sys.argv[4:]

SCRIPT_PATH = os.path.abspath(__file__)
SCRIPT_DIR = os.path.dirname(SCRIPT_PATH)

if sys.platform.startswith("openbsd"):
    cc = "egcc" # bugs in base compiler. uses loads of memory
else:
    cc = "gcc"

env = os.environ.copy()
env["CC"] = cc
env["PYTHONPATH"] = PYRO_DIR

print(">>>>%s" % sys.argv)
os.chdir(os.path.join(UNI_DIR, "pypy", "goal"))

args = [
    BOOTSTRAP_PY, "../../rpython/bin/rpython", "--output", OUTNAME,
    "--no-shared", "-Ojit", "targetpypystandalone.py"
] + extra_rpython_args

print(72 * ";")
print("TRANSLATING PYPY USING %s" % BOOTSTRAP_PY)
print(" ".join(args))
print(72 * ";")

os.execve(BOOTSTRAP_PY, args, env)
