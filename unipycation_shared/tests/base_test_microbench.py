import unittest, os
import pytest

#
# These tests were derived from crashes in the unipycation benchmark suite.
#
class BaseTestMicroBench(object):
    """ Tests the Highlevel uni.py API sugar """

    def test_make_and_consume_chain_n_times(self):
        import uni
        e = uni.Engine("""
        make_chain(A, B) :- make_chain(A, end, B).
        make_chain(X, In, Out) :-
            X > 0,
            X0 is X - 1,
            X2 is X * 2,
            make_chain(X0, pair(X, X2, In), Out).
        make_chain(0, In, In).
        """)

        def consume_chain(chain):
            res = 0
            while chain != "end":
                assert chain.name == "pair"
                val, val2, chain = chain
                res += val2 - val
            return res

        for count in range(1, 10):
            correct = count * (count + 1) // 2
            tmp = e.db.make_chain(count, None)[0]
            res = consume_chain(tmp)
            assert res == correct

    def test_gen1(self):
        import uni
        e = uni.Engine("""
            gen1(X, 1) :- X >= 0.
            gen1(X, Y) :- X > 0, X0 is X - 1, gen1(X0, Y).
        """)

        for count in range(1, 10):
            for r in e.db.gen1.iter(count, None):
                assert r == (1, )

    def test_non_iter_query(self):
        import uni
        e = uni.Engine("make_chain(pair(3, pair(2, pair(1, end)))).")
        (sol, ) = e.db.make_chain(None) # boom

    def test_smallfunc(self):
        import uni
        e = uni.Engine("""
            multiply_and_add(X, Y, Z, Res) :- Res is X + Y * Z.
        """)

        i = 100
        while i > 0:
            i, = e.db.multiply_and_add(i, 1, -1, None)

    def test_loop1arg0result(self):
        import uni
        e = uni.Engine("""
            countdown(X) :- X > 0, X0 is X - 1, countdown(X0).
            countdown(0).
        """)
        for i in range(10):
            res = e.db.countdown(10)
            assert res == ()


    def test_loop1arg1result(self):
        import uni
        e = uni.Engine("""
            sum_up_to_n(A, B) :- sum_up_to_n(A, 0, B).
            sum_up_to_n(A, Accumulator, Result) :-
                A > 0,
                A0 is A - 1,
                Accumulator1 is Accumulator + A,
                sum_up_to_n(A0, Accumulator1, Result).
            sum_up_to_n(0, Result, Result).
        """)
        for i in range(10):
            res, = e.db.sum_up_to_n(10, None)
