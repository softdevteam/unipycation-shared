import pytest
from pytest import raises

class BaseTestUni(object):
    """ Tests the Highlevel uni.py API sugar """

    def setup_class(cls):
        raise NotImplementedError("You need to override this for your VM")

    def test_basic(self):
        import uni

        e = uni.Engine("f(666).")
        assert e.db.f(None) == (666, )

    def test_basic2(self):
        import uni

        e = uni.Engine("f(1, 2, 4, 8).")
        assert e.db.f(1, None, 4, None) == (2, 8)

    def test_from_file(self):
        import uni
        import os
        fd = self.fd
        fname = self.fname

        os.write(fd, "f(1,2,3).")
        os.close(fd)

        e = uni.Engine.from_file(fname)
        os.unlink(fname)

        sol = e.db.f(None, None, None)
        assert sol == (1, 2, 3)

    def test_tautology(self):
        import uni

        e = uni.Engine("f(1).")
        sol = e.db.f(1)
        assert sol == tuple()

    def test_contradiction(self):
        import uni

        e = uni.Engine("f(1).")
        sol = e.db.f(2)
        assert sol == None

    def test_iter1(self):
        import uni
        e = uni.Engine("f(1). f(2). f(3).")

        expect = 1
        for (x, ) in e.db.f.iter(None):
            assert x == expect
            expect += 1

    def test_iter2(self):
        import uni
        e = uni.Engine("f(1, 2). f(2, 3). f(3, 4).")

        xe = 1; ye = 2
        for (x, y) in e.db.f.iter(None, None):
            assert (x, y) == (xe, ye)
            xe += 1
            ye += 1

    def test_iter3(self):
        import uni
        e = uni.Engine("f(card(5, d)). f(card(6, c)).")

        sols = [ x for (x, ) in e.db.f.iter(None) ]
        expect = [ e.terms.card(5, "d"), e.terms.card(6, "c") ]

        assert sols == expect

    def test_term_getattrs(self):
        import uni

        e = uni.Engine("f(1,2,3).")
        assert e.terms.f(1, 2, 3) == uni.Term("f", [1, 2, 3])

    def test_term_getattrs2(self):
        import uni

        e = uni.Engine("f(1,2,3).")

        t1 = e.terms.g(666, 667, 668)
        t =  e.terms.f(1, 2, t1)

        assert t == uni.Term("f", [1, 2, uni.Term("g", [666, 667, 668])])

    def test_recursive_listconv(self):
        import uni

        e = uni.Engine("g(f([212, 213, 414])).")
        x, = e.db.g(None)
        assert x[0] == [212, 213, 414]

    def test_convert_inside_list(self):
        import uni
        e = uni.Engine("g([[1], [2], [3]]).")
        x = e.db.g([[1], [2], [3]])
        assert x == ()

        e = uni.Engine("g([[1], [2], [3]]).")
        x, = e.db.g(None)
        assert x == [[1], [2], [3]]

    def test_undefined_goal(self):
        import uni
        e = uni.Engine("f(1,2,3).")
        info = raises(uni.PrologError, lambda : e.db.g(666))

    def test_append(self):
        import uni

        e = uni.Engine("""
            app([], X, X).
            app([H | T1], T2, [H | T3]) :- app(T1, T2, T3).
        """)
        res = e.db.app([1, 2, 3, 4], [7, 8, 9], None)
        assert res == ([1, 2, 3, 4, 7, 8, 9], )


    def test_emptylist(self):
        import uni

        e = uni.Engine("f([]).")
        (res, ) = e.db.f(None)
        assert res == []

    def test_undefined_list_tail(self):
        import uni
        e = uni.Engine("app([], X, X). app([H | T1], T2, [H | T3]).")

        def should_fail(e):
            # the second solution will have a list like [1|_G0]
            for (x, y) in e.db.app.iter(None, None, [1, 2, 3, 4, 5]): pass

        raises(uni.InstantiationError, lambda : should_fail(e))

    def test_weird_list(self):
        import uni
        e = uni.Engine("f('.'(a, b)). g([[1], d | e]).")
        result, = e.db.f(None)
        assert result == uni.Term('.', ["a", "b"])
        result, = e.db.g(None)
        assert result == uni.Term('.', [[1], uni.Term('.', ['d', 'e'])])


    def test_append_nondet(self):
        import uni

        e = uni.Engine("""
            app([], X, X).
            app([H | T1], T2, [H | T3]) :- app(T1, T2, T3).
        """)

        for (x, y) in e.db.app.iter(None, None, [1, 2, 3, 4, 5]):
            assert(type(x) == list)
            assert(type(y) == list)
            assert x + y == [1, 2, 3, 4, 5]

    def test_reverse(self):
        import uni
        e = uni.Engine("""
            rev(X, Y) :- rev_helper(X, [], Y).
            rev_helper([], Acc, Acc).
            rev_helper([H | T], Acc, Res) :- rev_helper(T, [H | Acc], Res).
        """)
        assert e.db.rev([1, 2, 3, 4], None) == ([4, 3, 2, 1], )

    def test_unbound(self):
        import uni

        e = uni.Engine("f(X) :- X = g(_).")
        sol = e.db.f(None)
        assert len(sol[0].args) == 1
        assert type(sol[0].args[0]) == uni.Var

    def test_pass_up_prolog_error(self):
        import uni

        e = uni.Engine("f(X) :- willsmith(1, [1,2,3], X).")
        info = raises(uni.PrologError, e.db.f, None)
        assert info.value.term.name == "error"
        assert info.value.term[0].name == "existence_error"

    def test_collect_nones_recursively(self):
        import uni

        e = uni.Engine("f(g(1, 2)).")
        a, b = e.db.f(e.terms.g(None, None))
        assert a == 1
        assert b == 2

    def test_long_list(self):
        import uni

        e = uni.Engine("f(1).")
        a, = e.db.append([1] * 1000, [2] * 2000, None)
        assert a == [1] * 1000 + [2] * 2000

    def test_deep_data(self):
        import uni
        e = uni.Engine("""add1(X, s(X)).""")
        p1000 = "end"
        for i in range(1000):
            p1000 = e.terms.s(p1000)
        p1001, = e.db.add1(p1000, None)
        for i in range(1001):
            p1001 = p1001[0]
        assert p1001 == "end"

    def test_deep_data_through_lists(self):
        import uni
        e = uni.Engine("""add1(X, [X]).""")
        p1000 = "end"
        for i in range(1000):
            p1000 = [p1000]
        p1001, = e.db.add1(p1000, None)
        for i in range(1001):
            p1001 = p1001[0]
        assert p1001 == "end"

    def test_term_reuse(self):
        import uni
        e = uni.Engine("f(g(1)). f(g(2)). f(g(3)).")

        gx = e.terms.g(None)

        def use_gx(e, gx):
            expect = 1
            for (i, ) in e.db.f.iter(gx):
                assert i == expect
                expect += 1

        # Use gx for the first time.
        use_gx(e, gx)

        # Now check that gx still works for a second time.
        # In theory it should be fine as gx's insides are lazily
        # instantiated on the fly. If this were not the case, then
        # swithon's implementation would have swept the internal term
        # refs and data after the frame was closed.
        use_gx(e, gx)

class BaseTestUniRevCall(object):
    def test_call_python(self):
        import uni

        def f(x):
            return x + 1

        e = uni.Engine("f(X) :- python:f(15, X).", locals())
        x, = e.db.f(None)
        assert x == 16

    def test_call_python_convert_arg(self):
        import uni

        e = uni.Engine("f(X) :- python:len([1, 2, 3, 4, 5], X).", locals())
        x, = e.db.f(None)
        assert x == 5

    def test_call_python_convert_result(self):
        import uni

        def f(x):
            return len(x)

        e = uni.Engine("f(X) :- python:range(5, Y), length(Y, X).", locals())
        x, = e.db.f(None)
        assert x == 5

    def test_paper_cls_example(self):
        import uni

        e = uni.Engine("""
        baseclass(A, A).
        baseclass(A, B) :-
            python:getattr(A, '__base__', Base),
            baseclass(Base, B).

        hasmethod(A, Method) :-
            A:'__dict__':iterkeys(Method).

        baseclass_defining_method(A, Method, Base) :-
            baseclass(A, Base),
            hasmethod(Base, Method).

        """)

            #python:getattr(A, '__dict__', Dict),
            #contains(Dict, Method).

        class A(object):
            def f(self):
                pass

        class B(A):
            def g(self):
                pass

        class C(A):
            def f(self):
                pass

        assert e.db.baseclass_defining_method(B, 'f', None) == (A, )

    def test_list_in_term(self):
        import uni

        e = uni.Engine("f(g([c(0, 0)])).")
        (g, ) = e.db.f(None)
        assert isinstance(g, uni.Term)
        assert g.name == "g"
        assert len(g) == 1
        assert g[0] == [uni.Term('c', [0, 0])]
        assert isinstance(g.args[0], list)
