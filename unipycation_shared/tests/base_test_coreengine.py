import tempfile
import pytest
from pytest import raises

class BaseTestCoreEngine(object):

    def setup_class(cls):
        raise NotImplementedError("Implement this in the subclass")

    def test_basic(self):
        import uni as u

        e = u.CoreEngine("f(666).")
        X = u.Var()
        t = u.CoreTerm('f', [X])
        it = e.query_iter(t, [X])

        assert it.next()[X] == 666
        it.finalise()

    def test_from_file(self):
        import uni as u, os
        fd = self.fd
        fname = self.fname

        os.write(fd, "f(1,2,3).")
        os.fsync(fd)

        e = u.CoreEngine.from_file(fname)

        vs = [X, Y, Z] = [ u.Var() for x in range(3) ]

        t = u.CoreTerm('f', vs)
        it = e.query_iter(t, vs)
        sol  = it.next()

        assert sol[X] == 1
        assert sol[Y] == 2
        assert sol[Z] == 3

        it.finalise()


    def test_from_file_error(self):

        import uni as u
        import os
        fd = self.fd
        fname = self.fname
        # check parse error has file information
        os.write(fd, "\nf(1,2,3).\nf(1 + + - ^")
        os.fsync(fd)

        info = raises(u.ParseError, u.CoreEngine.from_file, fname)
        assert fname in str(info.value)
        os.unlink(fname)


    def test_from_file_nul(self):
        import uni
        raises(TypeError, uni.CoreEngine.from_file, "a\x00")

    def test_iterator(self):
        import uni as u

        e = u.CoreEngine("f(666, 667). f(222, 334). f(777, 778).")
        vs = [X, Y] = [u.Var(), u.Var()]

        t = u.CoreTerm('f', vs)
        it = e.query_iter(t, vs)

        s1 = it.next()
        s2 = it.next()
        s3 = it.next()

        assert(s1[X] == 666 and s1[Y] == 667)
        assert(s2[X] == 222 and s2[Y] == 334)
        assert(s3[X] == 777 and s3[Y] == 778)
        raises(StopIteration, lambda: it.next())

    def test_functors(self):
        import uni as u

        e = u.CoreEngine("f(a(1, 2), b(3, 4)).")
        vs = [X, Y] = [u.Var(), u.Var()]

        # f(a(1, X), Y)
        t1 = u.CoreTerm('a', [1, X])
        t = u.CoreTerm('f', [t1, Y])

        it = e.query_iter(t, vs)
        sol = it.next()
        expect_y = u.CoreTerm('b', [3, 4])
        not_expect_y = u.CoreTerm('zzz', ["oh", "no"])

        assert sol[X] == 2
        assert sol[Y]  == expect_y
        assert sol[Y] != not_expect_y # inequal terms
        assert sol[Y] != 1337         # term vs. number
        it.finalise()

    def test_tautology(self):
        import uni as u

        e = u.CoreEngine("eat(cheese, bread). eat(egg, salad).")

        t = u.CoreTerm('eat', ['cheese', 'bread'])
        it = e.query_iter(t, [])
        sol = it.next()

        assert len(sol) == 0
        it.finalise()

    def test_contradicion(self):
        import uni as u

        e = u.CoreEngine("eat(cheese, bread). eat(egg, salad).")
        t = u.CoreTerm('eat', ['cheese', 'egg'])
        it = e.query_iter(t, [])
        raises(StopIteration, "it.next()")

    def test_iterator_tautology(self):
        import uni as u

        e = u.CoreEngine("eat(cheese, bread). eat(egg, salad).")
        t = u.CoreTerm('eat', ['cheese', 'bread'])
        it = e.query_iter(t, [])
        sols = [ x for x in it ]
        sol, = sols
        assert len(sol) == 0

    def test_iterator_contradiction(self):
        import uni as u

        e = u.CoreEngine("eat(cheese, bread). eat(egg, salad).")
        t = u.CoreTerm('eat', ['cheese', 'egg'])
        it = e.query_iter(t, [])
        raises(StopIteration, lambda : it.next())

    def test_iterator_infty(self):
        import uni as u

        e = u.CoreEngine("""
                f(0).
                f(X) :- f(X0), X is X0 + 1.
        """)

        X = u.Var()
        t = u.CoreTerm('f', [X])
        it = e.query_iter(t, [X])

        first_ten = []
        for i in range(10):
            first_ten.append(it.next()[X])

        assert first_ten == range(0, 10)

    def test_query_nonexisting_predicate(self):
        import uni as u

        e = u.CoreEngine("f(666). f(667). f(668).")
        X = u.Var()
        t = u.CoreTerm("g", [X])

        raises(u.PrologError, lambda: e.query_iter(t, [X]).next())

    def test_iter_nonexisting_predicate(self):
        import uni as u

        e = u.CoreEngine("f(666). f(667). f(668).")
        X = u.Var()
        t = u.CoreTerm("g", [X])

        raises(u.PrologError, lambda: e.query_iter(t, [X]).next())

    def test_parse_db_incomplete(self):
        import uni

        raises(uni.ParseError, lambda: uni.CoreEngine("f(1)")) # missing dot on db

    def test_types_query(self):
        import uni

        e = uni.CoreEngine("eat(cheese, bread). eat(egg, salad).")
        v = uni.Var()
        raises(TypeError, lambda : e.query_iter(v, []))

    def test_types_query2(self):
        import uni

        e = uni.CoreEngine("eat(cheese, bread). eat(egg, salad).")
        t = uni.CoreTerm('eat', ['cheese', 'bread'])
        raises(TypeError, lambda : e.query_iter(t, [t]))

    def test_type_error_passed_up(self):
        import uni

        e = uni.CoreEngine("test(X) :- X is unlikely_to_ever_exist_ever(9).")
        X = uni.Var()
        t = uni.CoreTerm('test', [X])
        raises(uni.PrologError, lambda : e.query_iter(t, [X]).next())

    # (Pdb) p exc
    # Generic1(error, [Generic2(existence_error, [Atom('procedure'), Generic2(/, [Atom('select'), Number(3)])])])
    # This does not belong here XXX
    # XXX is this still a problem?
    def test_select(self):
        import uni

        e = uni.Engine("f(X) :- select(1, [1,2,3], X).")
        (res, ) = e.db.f(None)
        assert res == [2, 3]

    def test_unbound(self):
        import uni as u

        e = u.CoreEngine("f(X) :- X = g(_).")
        X = u.Var()
        t = u.CoreTerm('f', [X])
        it = e.query_iter(t, [X])
        sol = it.next()

        assert type(sol[X] == u.CoreTerm)
        assert len(sol[X].args) == 1
        assert type(sol[X].args[0] == u.Var)

        it.finalise()

    def test_error_in_database(self):
        import uni
        raises(uni.PrologError, 'uni.CoreEngine(":- foo, !.")')

    def test_fail_in_database(self):
        import uni
        raises(uni.PrologError, 'uni.CoreEngine(":- a = b.")')

    def test_collect_solutions(self):
        import uni as u
        e = u.CoreEngine("f(card(5, d)). f(card(6, c)).")
        # f(X).
        X = u.Var()
        t = u.CoreTerm("f", [X])

        it = e.query_iter(t, [X])
        sol = it.next()
        assert sol[X].name == "card"
        assert len(sol[X]) == 2
        assert sol[X][0] == 5
        assert sol[X][1] == "d"

        sol = it.next()
        assert sol[X].name == "card"
        assert len(sol[X]) == 2
        assert sol[X][0] == 6
        assert sol[X][1] == "c"

    def test_term_reuse(self):
        import uni

        e = uni.Engine("f(card(5, D)) :- g(D). g(d). g(c).")

        sols = [ x for x, in e.db.f.iter(None) ]

        assert sols[0].name == "card"
        assert len(sols[0]) == 2
        assert sols[0][0] == 5
        print(sols[0][1]) # prints "c"
        assert sols[0][1] == "d" # BOOM

        assert sols[1].name == "card"
        assert len(sols[1]) == 2
        assert sols[1][0] == 5
        assert sols[1][1] == "c"

    def test_preds_not_shared_between_coreengines(self):
        import uni

        e1 = uni.CoreEngine("f(1).")
        e2 = uni.CoreEngine("f(2).")

        t1 = uni.CoreTerm("f", [2])
        it1 = e1.query_iter(t1, [])
        raises(StopIteration, "it1.next()")

        t2 = uni.CoreTerm("f", [1])
        it2 = e2.query_iter(t2, [])
        raises(StopIteration, "it2.next()")

    def test_concurrent_queries(self):
        import uni
        e1 = uni.CoreEngine("f(1). f(2). f(3). f(4). f(5).")
        e2 = uni.CoreEngine("g(5). g(4). g(3). g(2). g(1).")

        (X, Y) = (uni.Var(), uni.Var())
        t1 = uni.CoreTerm("f", [X])
        t2 = uni.CoreTerm("g", [Y])

        it1 = e1.query_iter(t1, [X])
        it2 = e2.query_iter(t2, [Y])

        for i in range(1, 6):
            assert it1.next()[X] == i # swithon: explodes on second iteration
            assert it2.next()[Y] == 6 - i

        raises(StopIteration, "it1.next()")
        raises(StopIteration, "it2.next()")

    def test_conversion_subclasses(self):
        import uni

        # Don't seemt to be able to get as is/2 in tuProlog
        e = uni.Engine("myis(X, Y) :- X is Y.")

        class MyInt(int): pass

        x = MyInt(666)
        y, = e.db.myis(None, x)

        assert y == 666
        assert type(y) is int
