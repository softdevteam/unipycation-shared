import sys

# This is used in tests to identify which VM you are
IDENT = None

# Import core data structures
try:
    from unipycation import CoreTerm, Var, CoreEngine, PrologError, ParseError
    IDENT = "unipycation"
except ImportError:
    try:
        from jythog import CoreTerm, Var, CoreEngine, PrologError, ParseError
        IDENT = "jython"
    except ImportError:
        try:
            from swithon import CoreTerm, Var, CoreEngine, PrologError, ParseError
            IDENT = "swithon"
        except ImportError:
            ImportError("can't find unipycation or jythog or swithon")

class InstantiationError(Exception): pass

class Term(object):
    def __init__(self, symbol, args):
        self.name = symbol
        self.args = tuple(args)

    def __eq__(self, other):
        if type(self) != type(other):
            return False
        if self.name != other.name:
            return False
        return self.args == other.args

    def __ne__(self, other):
        return not self == other

    def __getitem__(self, index):
        return self.args[index]

    def __len__(self):
        return len(self.args)

    @staticmethod
    def _from_term(t):
        assert isinstance(t, CoreTerm)
        args = tuple(unrolling_map(Predicate._back_to_py, t.args))
        return Term(t.name, args)

    def __str__(self):
        return "%s(%s)" % (self.name, ", ".join([str(a) for a in self.args]))
    __repr__ = __str__

class _buildterm(tuple):
    def __new__(cls, name, numargs):
        return tuple.__new__(cls, (name, numargs))

class _buildlist(int):
    pass

class _buildtermlist(_buildlist):
    pass


# the next two functions convert Prolog terms to and from Python
# non-recursively using traditional defunctionalized continuations. the
# continuations are the classes _buildlist, _buildterm, _buildtermlist todo is
# the stack of expressions still to convert, res the list of already converted
# things.

# When todo is a continuation (instead of a value) the continuation takes some
# stuff of the res list and builds something else with it (a list, a term and a
# cons list, respectively)

def _convert_to_prolog_nonrecursive(e, collect_nones_as_vars=None):
    tp = type(e)
    if tp is int or tp is str: # fast path for two very common cases
        return e
    # this is the peeled simplified first iteration of the loop
    if e is None and collect_nones_as_vars is not None:
        v = Var()
        collect_nones_as_vars.append(v)
        return v
    if isinstance(e, list):
        todo = [_buildlist(len(e))]
        todo.extend(reversed(e))
    elif isinstance(e, Term):
        todo = [_buildterm(e.name, len(e))]
        # the reversing is done to that variables end up in
        # collect_nones_as_vars in the right order
        todo.extend(reversed(e))
    else:
        return e
    res = []
    while todo:
        e = todo.pop()
        if isinstance(e, _buildterm):
            name, numargs = e
            args = [res.pop() for i in range(numargs)]
            args.reverse()
            res.append(CoreTerm(name, args))
        elif isinstance(e, _buildlist):
            out = "[]"
            for i in range(e):
                elem = res.pop()
                out = CoreTerm(".", [elem, out])
            res.append(out)
        elif e is None and collect_nones_as_vars is not None:
            v = Var()
            collect_nones_as_vars.append(v)
            res.append(v)
        elif isinstance(e, list):
            todo.append(_buildlist(len(e)))
            todo.extend(reversed(e))
        elif isinstance(e, Term):
            todo.append(_buildterm(e.name, len(e)))
            # the reversing is done to that variables end up in
            # collect_nones_as_vars in the right order
            todo.extend(reversed(e))
        else:
            res.append(e)
    assert len(res) == 1
    return res[0]

def _convert_to_python_nonrecursive(e):
    # peel first iteration of below loop and specialise
    if e == "[]":
        return []
    elif (not isinstance(e, CoreTerm)):
        #res.append(e)
        return e
    elif e.name == ".":
        buildlistindex = 0
        todo = [None]
        curr = e
        result = []
        length = 0
        while True:
            if isinstance(curr, Var): # the rest of the list is unknown
                raise InstantiationError("The tail of a list was undefined")
            if curr == "[]": # end of list
                todo[buildlistindex] = _buildlist(length)
                break
            if not isinstance(curr, CoreTerm) or not curr.name == ".":
                assert not isinstance(curr, Term) # should not see high-level term
                # malformed list
                todo.append(curr)
                todo[buildlistindex] = _buildtermlist(length + 1)
                break
            todo.append(curr[0])
            curr = curr[1]
            length += 1
    else:
        # CoreTerm
        todo = [ _buildterm(e.name, len(e)) ]
        todo.extend(e)


    todo = [e]
    res = []
    while todo:
        e = todo.pop()
        if isinstance(e, _buildterm):
            name, numargs = e
            args = [res.pop() for i in range(numargs)]
            res.append(Term(name, args))
        elif isinstance(e, _buildlist):
            l = res[-e:]
            del res[-e:]
            if not isinstance(e, _buildtermlist):
                l.reverse()
                res.append(l)
            else:
                curr = l[0]
                for i in range(1, e):
                    curr = Term(".", [l[i], curr])
                res.append(curr)
        elif e == "[]":
            res.append([])
        elif (not isinstance(e, CoreTerm)):
            res.append(e)
        elif e.name == ".":
            buildlistindex = len(todo)
            todo.append(None) # where to put _buildlist later
            curr = e
            result = []
            length = 0
            while True:
                if isinstance(curr, Var): # the rest of the list is unknown
                    raise InstantiationError("The tail of a list was undefined")
                if curr == "[]": # end of list
                    todo[buildlistindex] = _buildlist(length)
                    break
                if not isinstance(curr, CoreTerm) or not curr.name == ".":
                    assert not isinstance(curr, Term) # should not see high-level term
                    # malformed list
                    todo.append(curr)
                    todo[buildlistindex] = _buildtermlist(length + 1)
                    break
                todo.append(curr[0])
                curr = curr[1]
                length += 1
        else:
            # CoreTerm
            todo.append(_buildterm(e.name, len(e)))
            for arg in e:
                todo.append(arg)
    assert len(res) == 1
    return res[0]

class Engine(CoreEngine):
    """ A subclass of unipycation.CoreEngine adding .db and .terms convenience
    features. """

    def __new__(cls, db_str, ns=None, filename=None):
        if IDENT == "unipycation":
            if ns is None:
                ns = sys._getframe(1).f_globals
        self = CoreEngine.__new__(cls, db_str, ns, filename)
        self.db = Database(self)
        self.terms = TermPool()
        return self

    @staticmethod
    def _convert_to_prolog(e):
        return Predicate._convert_to_prolog(e)

    @staticmethod
    def _back_to_py(e):
        return Predicate._back_to_py(e)

class SolutionIterator(object):
    """ A wrapper around unipycation.CoreSolutionIterator. """
    def __init__(self, it):
        self.it = it

    def __iter__(self): return self

    def next(self):
        sol = self.it.next()
        return Predicate._make_result_tuple(sol)

class Predicate(object):
    """ Represents a "callable" prolog predicate """

    def __init__(self, engine, name):
        self.engine = engine
        self.name = name

    @staticmethod
    def _convert_to_prolog(e, collect_nones_as_vars=None):
        return _convert_to_prolog_nonrecursive(e, collect_nones_as_vars)

    @staticmethod
    def _back_to_py(e):
        return _convert_to_python_nonrecursive(e)

    @staticmethod
    def _make_result_tuple(sol):
        values = sol.get_values_in_order()
        return tuple(unrolling_map(Predicate._back_to_py, values))

    def __call__(self, *args):
        vs = []
        def convert(e):
            return self._convert_to_prolog(e, vs)
        term_args = unrolling_map(convert, args)
        t = CoreTerm(self.name, term_args)
        return self._actual_call(t, vs)

    def _actual_call(self, t, vs):
        it = self.engine.query_iter(t, vs)

        try:
            sol = it.next()
        except StopIteration:
            return None # contradiction, iterator already finalised

        # otherwise copy the solution and finalise the iterator
        rv = Predicate._make_result_tuple(sol)
        it.finalise()
        return rv

    @property
    def iter(self):
        return IterPredicate(self.engine, self.name)

    @property
    def many_solutions(self):
        raise NotImplementedError("deprecated")

class IterPredicate(Predicate):
    def _actual_call(self, t, vs):
        it = self.engine.query_iter(t, vs)
        return SolutionIterator(it)

    @property
    def iter(self):
        return self

class Database(object):
    """ A class that represents the predicates exposed by a prolog engine """

    def __init__(self, engine):
        self.engine = engine

    def __getattr__(self, name):
        """ Predicates are called by db.name and are resolved dynamically """
        pred = Predicate(self.engine, name)
        return pred

class TermPool(object):
    """ Represents the term pool, some magic to make term creation prettier """

    def __getattr__(self, name):
        # Note that we cant memoise these due to the args being variable
        return lambda *args : Term(name, args)


def unrolling_map(fun, sequence):
    """ This function behaves like a simple version of map, taking a function
    of one argument, and a sequence. The added complication over map is that it
    will unroll the loop for small lists. The benefit is that for the short
    cases, the JIT does not see the loop and thus the construction of the
    result is completely transparent to it. """
    length = len(sequence)
    if length == 0:
        return []
    elif length == 1:
        return [fun(sequence[0])]
    elif length == 2:
        return [fun(sequence[0]), fun(sequence[1])]
    elif length == 3:
        return [fun(sequence[0]), fun(sequence[1]), fun(sequence[2])]
    elif length == 4:
        return [fun(sequence[0]), fun(sequence[1]), fun(sequence[2]),
                fun(sequence[3])]
    elif length == 5:
        return [fun(sequence[0]), fun(sequence[1]), fun(sequence[2]),
                fun(sequence[3]), fun(sequence[4])]
    return map(fun, sequence)

