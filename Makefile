PYTHON=python2.7
# empty means all
VMS?=
PY_DEPS="sh vcstools"

all:
	${PYTHON} bootstrap.py all ${VMS}

fetch:
	${PYTHON} bootstrap.py fetch ${VMS}

build:
	${PYTHON} bootstrap.py build ${VMS}

configure:
	${PYTHON} bootstrap.py configure ${VMS}
